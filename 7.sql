SELECT customer.country, COUNT(*)
FROM customer
GROUP BY country
ORDER BY count DESC;
