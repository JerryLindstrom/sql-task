SELECT customer.customer_id, first_name, last_name, country, postal_code, phone, email
FROM customer
WHERE customer.first_name LIKE 'Frank'

UPDATE customer SET first_name = 'Jerry'
WHERE first_name = 'Frank'

UPDATE customer SET country = 'Sweden'
WHERE customer_id = 10 OR customer_id = 20 OR customer_id = 30

SELECT customer.customer_id, first_name, last_name, country, postal_code, phone, email
FROM customer
WHERE customer.first_name LIKE 'Jerry'