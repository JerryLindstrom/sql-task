SELECT customer.first_name, customer.last_name, SUM(invoice.total)
FROM customer
INNER JOIN invoice ON invoice.customer_id = customer.customer_id
GROUP BY customer.customer_id
ORDER BY sum DESC;
