SELECT
first_name,
last_name,
genre."name",
COUNT(genre."name")
FROM customer
INNER JOIN invoice ON invoice.customer_id = customer.customer_id
INNER JOIN invoice_line ON invoice_line.invoice_id = invoice.invoice_id
INNER JOIN track ON track.track_id = invoice_line.track_id
INNER JOIN genre ON genre.genre_id = track.genre_id
WHERE customer.customer_id = '12'
GROUP BY genre."name", customer.first_name, customer.last_name
ORDER BY COUNT DESC;
