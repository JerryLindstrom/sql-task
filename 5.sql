INSERT INTO customer (first_name, last_name, country, postal_code, phone, email)
VALUES ('Jerry', 'Lindström', 'Sweden', '64533', '0152-12345', 'jerry.lindstrom@scania.com' )

SELECT customer.customer_id, first_name, last_name, country, postal_code, phone, email
FROM customer
WHERE customer.first_name LIKE 'Jerry'